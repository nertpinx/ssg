Simple static site generation template with deployment to GitLab Pages using
CI/CD pipelines.  To see the URL for your pages go to Settings → Pages.  This
particular one is hosted on https://nertpinx.gitlab.io/ssg
